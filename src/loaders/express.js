'use strict'

const config = require('../config')
const helmet = require('helmet')
const bodyParser = require('body-parser')
const routes = require('../routes')

module.exports = async ({ app }) => {
  app.use(bodyParser.urlencoded({ extended: false }))
  app.use(helmet())
  // Routes
  app.use(config.api.prefix, routes)

  // catch 404 and forward to error handler
  app.use((req, res, next) => {
    const err = new Error('Not Found')
    err['status'] = 404
    next(err)
  })

  // Error hanlder
  app.use((err, req, res, next) => {
    /**
     * Handle 401 thrown by express-jwt library
     */
    if (err.name === 'UnauthorizedError') {
      return res
        .status(err.status)
        .send({ message: err.message })
        .end()
    }
    return next(err)
  })
  app.use((err, req, res, next) => {
    res.status(err.status || 500)
    res.json({
      errors: {
        message: err.message
      }
    })
  })

  return app
}
