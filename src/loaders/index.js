'use strict'

const mongooseLoader = require('./mongoose')
const expressLoader = require('./express')

module.exports = async ({ expressApp }) => {
  await mongooseLoader()
  console.log('MongoDB Initialized')
  await expressLoader({ app: expressApp })
  console.log('Express Initialized')
}
