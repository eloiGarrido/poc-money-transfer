'use strict'

const mongoose = require('mongoose')
const config = require('../config')

module.exports = async () => {
  const conn = await mongoose.connect(
    config.db.uri,
    config.db.options
  )
  return conn.connection.db
}
