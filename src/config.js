'use strict'

require('dotenv').config()

// Default NODE_ENV to 'development'
process.env.NODE_ENV = process.env.NODE_ENV || 'development'

module.exports = {
  port: process.env.PORT || 3000,
  jwtSecret: process.env.TOKEN_SECRET || 'randomSecretString',
  db: {
    uri: process.env.DB_URI || 'mongodb://localhost:27017/moneyTrans',
    options: {
      useNewUrlParser: true,
      useCreateIndex: true
    }
  },

  api: {
    prefix: '/api/v1'
  }
}
