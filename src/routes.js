'use strict'

const { Router } = require('express')
const auth = require('./api/routes/auth')
const user = require('./api/routes/user')

const app = Router()
auth(app)
user(app)

module.exports = app
