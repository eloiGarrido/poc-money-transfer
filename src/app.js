'use strict'

const config = require('./config')
const express = require('express')
const loaders = require('./loaders/index')

async function startServer () {
  try {
    const app = express()
    await loaders({ expressApp: app })

    app.listen(config.port, err => {
      if (err) {
        console.error(err)
        process.exit(1)
      }
      console.log(`Server listening on port ${config.port}`)
    })

    return app
  } catch (e) {
    console.error(e)
    process.exit(1)
  }
}

module.exports = startServer()
