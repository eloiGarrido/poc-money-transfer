'use strict'

const { Router } = require('express')
const route = Router()
const { userService } = require('../services/index')
const middleware = require('../middlewares/index')
const { validationResult, checkSchema } = require('express-validator/check')
const { depositSchema, transferSchema } = require('../schemas/schemas')

module.exports = app => {
  app.use('/user', route)

  route.get('/me', middleware.isAuth, middleware.attachUser, (req, res) => {
    const user = req.currentUser
    Reflect.deleteProperty(user, '_id')
    return res.json({ user }).status(200)
  })

  // Get user balance
  route.post(
    '/balance',
    middleware.isAuth,
    middleware.attachUser,
    async (req, res, next) => {
      try {
        // Look for input validation errors
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
          return res.status(422).json({ errors: errors.array() })
        }

        // Retrieve user balance
        const balance = await userService.getBalance(req.currentUser._id)
        // Return user balance
        res.json(balance)
      } catch (e) {
        console.error(e)
        next(e)
      }
    }
  )

  route.post(
    '/deposit',
    middleware.isAuth,
    middleware.attachUser,
    checkSchema(depositSchema),
    async (req, res, next) => {
      try {
        // Look for input validation errors
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
          return res.status(422).json({ errors: errors.array() })
        }

        const userId = req.currentUser._id
        const balance = {
          amount: req.body.amount,
          currency: req.body.currency
        }

        const userRecord = await userService.addBalance(userId, balance)
        const userBalance = userRecord.toObject()
        // Parse balance object
        res.json({
          amount: userBalance.amount,
          currency: userBalance.currency,
          updatedAt: userBalance.updatedAt
        })
      } catch (e) {
        console.error(e)
        next(e)
      }
    }
  )

  // Transfer balance between users
  route.post(
    '/transfer',
    middleware.isAuth,
    middleware.attachUser,
    checkSchema(transferSchema),
    async (req, res, next) => {
      try {
        // Look for input validation errors
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
          return res.status(422).json({ errors: errors.array() })
        }

        const srcUser = req.currentUser._id
        const destEmail = req.body.dest_email
        const balance = {
          amount: req.body.amount,
          currency: req.body.currency,
          concept: req.body.concept || ''
          // execDate: Date() // Could add an execution date
        }
        // Authenticate user
        const userRecord = await userService.transferBalance(
          srcUser,
          destEmail,
          balance
        )
        const userBalance = userRecord.toObject()
        // Parse balance object
        res.json({
          amount: userBalance.amount,
          currency: userBalance.currency,
          updatedAt: userBalance.updatedAt
        })
      } catch (e) {
        console.error(e)
        next(e)
      }
    }
  )
}
