'use strict'

const { Router } = require('express')
const { validationResult } = require('express-validator/check')
const { authService } = require('../services/index')
const { checkSchema } = require('express-validator/check/index')
const { signinSchema, signupSchema } = require('../schemas/schemas')
const route = Router()

module.exports = app => {
  app.use('/auth', route)

  route.post('/signup', checkSchema(signupSchema), async (req, res, next) => {
    try {
      // Look for input validation errors
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
      }
      // Signup new user
      const { user, token } = await authService.signUp(req.body)
      return res.json({ user, token }).status(201)
    } catch (e) {
      console.error(e)
      return next(e)
    }
  })

  route.post('/signin', checkSchema(signinSchema), async (req, res, next) => {
    try {
      // Look for input validation errors
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
      }
      // Retrieve existing user
      const { email, password } = req.body
      const { user, token } = await authService.signIn(email, password)
      return res.json({ user, token }).status(200)
    } catch (e) {
      console.error(e)
      return next(e)
    }
  })
}
