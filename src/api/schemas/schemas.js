'use strict'
// Schema definitions
const name = {
  in: ['body'],
  isAlpha: true,
  exists: true,
  trim: true
}

const email = {
  in: ['body'],
  isEmail: true,
  exists: true,
  trim: true
}

const password = {
  in: ['body'],
  exists: true
}

const currency = {
  in: ['body'],
  exists: true,
  isAlpha: true
}

const amount = {
  in: ['body'],
  isFloat: true,
  exists: true,
  toFloat: true
}

const concept = {
  in: ['body'],
  optional: true,
  trim: true,
  stripLow: true
}

// Schema interfaces
const signupSchema = {
  name,
  email,
  password
}

const signinSchema = {
  email,
  password
}

const transferSchema = {
  dest_email: email,
  currency,
  amount,
  concept
}

const depositSchema = {
  amount,
  currency
}

module.exports = {
  signupSchema,
  signinSchema,
  transferSchema,
  depositSchema
}
