'use strict'

const jwt = require('express-jwt')
const config = require('../../config')

function getToken (req) {
  const authHeader = req.headers.authorization

  if (
    (authHeader && authHeader.split(' ')[0] === 'Token') ||
    (authHeader && authHeader.split(' ')[0] === 'Bearer')
  ) {
    return authHeader.split(' ')[1]
  }
  return null
}

const isAuth = jwt({
  secret: config.jwtSecret, // The _secret_ to sign the JWTs
  userProperty: 'token', // Use req.token to store the JWT
  getToken: getToken // How to extract the JWT from the request
})

module.exports = isAuth
