'use strict'

const { User } = require('../../models/index')

async function attachUser (req, res, next) {
  try {
    const userRecord = await User.findById(req.token._id).select(
      '_id name email'
    )
    if (!userRecord) {
      return res.sendStatus(401)
    }
    const currentUser = userRecord.toObject()
    req.currentUser = currentUser
    return next()
  } catch (e) {
    console.error('Error attaching user', e)
    next()
  }
}

module.exports = attachUser
