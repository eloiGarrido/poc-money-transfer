'use strict'

const attachUser = require('./attachUser')
const isAuth = require('./auth')

module.exports = {
  attachUser,
  isAuth
}
