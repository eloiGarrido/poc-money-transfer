'use strict'

module.exports = {
  userService: require('./user'),
  authService: require('./auth')
}
