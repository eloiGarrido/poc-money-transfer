'use strict'
const { Balance, User, Transference } = require('../../models/index')

async function createTransfer (src, dest, balance) {
  return Transference.create({
    _user: src,
    _dest: dest,
    currency: balance.currency,
    amount: balance.amount,
    concept: balance.concept,
    status: 'open'
  })
}

async function setTransferStatus (transfer, status) {
  transfer.status = status
  transfer.save()
}

async function getBalance (user) {
  // Get all balance entries for a specific user
  return Balance.find({ _user: user }).select('amount currency updatedAt -_id')
}

async function addBalance (user, balance) {
  // Find if there's an already existing record
  const balanceRecord = await Balance.findOne({
    currency: balance.currency,
    _user: user
  })
  // Create transference record
  const transfer = await createTransfer(user, user, balance)
  if (!transfer) {
    throw new Error('Error registering transference')
  }
  // Balance found, modify balance
  if (balanceRecord) {
    balanceRecord.amount += balance.amount
    await setTransferStatus(transfer, 'closed')
    return balanceRecord.save()
  }
  // No record found, create a new entry and save
  const newBalance = await Balance.create({
    currency: balance.currency,
    amount: balance.amount,
    _user: user
  })
  if (!newBalance) {
    throw new Error('Error creating balance record')
  }

  await setTransferStatus(transfer, 'closed')
  return newBalance.save()
}

async function transferBalance (srcUser, destEmail, balance) {
  // Find if there's an already existing record
  const balanceRecord = await Balance.findOne({
    currency: balance.currency,
    _user: srcUser
  })
  // User does not have a currency balance or not enough
  /**
   * In this implementation we only check for enough balance on the specific
   * currency, we do not swap between currencies (EUR<>USD) or request the bank
   */
  if (!balanceRecord || balanceRecord.amount < balance.amount) {
    throw new Error('Not enough balance')
  }

  const destUserRecord = await User.find({ email: destEmail })
  if (!destUserRecord) {
    throw new Error('Destination user does not exist')
  }
  // Create transference record
  const transfer = await createTransfer(srcUser, destUserRecord._id, balance)

  // Remove balance from original account
  balanceRecord.amount -= balance.amount
  const userBalance = await balanceRecord.save()

  try {
    // Transfer balance to destination account
    await addBalance(destUserRecord._id, balance)
    await setTransferStatus(transfer, 'closed')
    return userBalance
  } catch (e) {
    console.error(e)
    // Revert transaction
    balanceRecord.amount += balance.amount
    await balanceRecord.save()
    await setTransferStatus(transfer, 'error')
    throw e
  }
}

module.exports = {
  getBalance,
  addBalance,
  transferBalance
}
