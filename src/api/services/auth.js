'use strict'
const argon2 = require('argon2')
const jwt = require('jsonwebtoken')
const { randomBytes } = require('crypto')
const { User } = require('../../models/index')
const config = require('../../config')

function generateToken (user) {
  const today = new Date()
  const exp = new Date(today)

  exp.setDate(today.getDate() + 60)

  return jwt.sign(
    {
      _id: user._id,
      name: user.name,
      exp: exp.getTime() / 1000
    },
    config.jwtSecret
  )
}

async function signUp ({ name, email, password }) {
  try {
    const salt = randomBytes(32)
    const hashedPassword = await argon2.hash(password, { salt })
    const userRecord = await User.create({
      name,
      email,
      salt,
      password: hashedPassword
    })
    // Generate user token
    const token = generateToken(userRecord)

    if (!userRecord) {
      throw new Error('Error creating user')
    }

    const user = userRecord.toObject()
    Reflect.deleteProperty(user, 'password')
    Reflect.deleteProperty(user, 'salt')
    Reflect.deleteProperty(user, '_id')

    return { user, token }
  } catch (e) {
    console.error(e)
    throw e
  }
}

async function signIn (email, password) {
  const userRecord = await User.findOne({ email }).select(
    '-createdAt -updatedAt -__v'
  )

  if (!userRecord) {
    throw new Error('User not registered')
  }
  // Prefent timing based attacks
  const validPassword = await argon2.verify(userRecord.password, password)

  if (validPassword) {
    const token = generateToken(userRecord)

    const user = userRecord.toObject()
    Reflect.deleteProperty(user, 'password')
    Reflect.deleteProperty(user, 'salt')
    Reflect.deleteProperty(user, '_id')

    return { user, token }
  } else {
    throw new Error('Invalid password')
  }
}

module.exports = {
  signUp,
  signIn
}
