'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const balanceSchema = new Schema(
  {
    _user: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    currency: {
      type: String,
      require: [true, 'Please enter valid currency'],
      index: true
    },

    amount: {
      type: Number,
      default: 0,
      min: 0
    }
  },
  { timestamps: true }
)

module.exports = mongoose.model('Balance', balanceSchema)
