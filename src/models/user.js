'use strict'

const mongoose = require('mongoose')
const validator = require('validator')
const Schema = mongoose.Schema

const userSchema = new Schema(
  {
    name: {
      type: String,
      required: [true, 'Please enter a full name'],
      index: true
    },

    email: {
      type: String,
      unique: true,
      index: true,
      lowercase: true,
      validate: value => {
        return validator.isEmail(value)
      }
    },

    password: String,

    salt: String
  },
  { timestamps: true }
)

module.exports = mongoose.model('User', userSchema)
