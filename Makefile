IMAGE=money-transfer

image:
	@docker build -t $(IMAGE) -f ./Dockerfile .

build:
	docker-compose build --no-cache
	@make -s clean

start:
	docker-compose up -d

stop:
	docker-compose stop

clean:
	@docker system prune --volumes --force