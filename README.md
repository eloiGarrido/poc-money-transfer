# Money Transfer
NodeJs, ExpressJs, and MongoDB based proof of concept for a money transfer backend application. 

The application uses the `Authorization` header field to carry a JWT token which authenticates the user to query information from the system.
To obtain the session token a new user can register or login with email and password.
Once the client holds the token, this can be used to access the server features linked to the token holder account. The currently implemented options are: user information `/user/me`, account balances `user/balance` and transfer balance to another account `/user/transfer`.

## Setup
To run the server locally clone the repository and install
```
git clone git@gitlab.com:eloiGarrido/poc-money-transfer.git
cd poc-money-transfer
npm install
```
The server requires a `mongod` instance running. Install MongoDB and boot up the database with `mongod` command.

To start the server execute `npm run start`, or use `npm run dev` if you want auto-reloading on file changes. The server will be exposed on `http://localhost:3000`

Alternatively, you can use docker to spin up the whole infrastructure.
A Makefile has been provided with the Docker commands in it.
```
make build
make start
make stop
```
## Endpoints
- [Authorization](#auth)
  - [Sign up](#signUp)
  - [Sign in](#signIn)
- [User data](#userData)
  - [User information](#me)
  - [Desposit](#deposit)
  - [Balance](#balance)
  - [Transfer](#transfer)

### <a name="auth"></a> Authorization API
Both endpoints are serve the purpose of obtaining a session JSON web token with which to authenticate a user.
Both endpoints will reply with errors if any of the expected body fields are missing or there is an error while creating/retrieving a specific user (i.e. attempting to create an account with an already existing email).
#### <a name="signUp"></a> Sign up: POST `/api/v1/auth/signup`
Create a new user based on email (id), name and password. Returns token used to authenticate further client-server operations
- Request:
  ```
  {
    status: 200,
    headers:{
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    },
    body:{
      email: {string}
      name: {string}
      password: {string}
    }
  }
  ```
- Reply
  ```
  {
    status: 200,
    headers:{
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    },
    body:{
      "user": {
          "name": "User name",
          "email": "Email",
          "createdAt": "2019-05-26T17:07:55.960Z",
          "updatedAt": "2019-05-26T17:07:55.960Z",
          "__v": 0
      },
      "token": "jwt token"
    }
  }
  ```
#### <a name="signIn"></a> Sign in: POST `/api/v1/auth/signup`
Authenticate an already existing user with email and password. Returns token used to authenticate further client-server opperations.
- Request:
  ```
  {
    status: 200,
    headers:{
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    },
    body:{
      email: {string}
      password: {string}
    }
  }
  ```
- Reply
  ```
  {
    status: 200,
    headers:{
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    },
    body:{
      "user": {
        "name": "User name",
        "email": "email"
      }
    },
      "token": "jwt token"
    }
  }
  ```
### <a name="userData"></a> User data API
Once the client is in possession of the session token, he/she is allowed to access the server features linked to its account. If the JWT is missing, expired or invalid, the server will return an error and stop any further operation.

Even though not exposed, any action that modifies the balance of an account creates a `transference` record. This transference specifies origin, target, amount, currency, concept, date and status. The status field can be used to track faulty transactions or bugs.
#### <a name="me"></a> User information: GET `/api/v1/user/me`
Expects a user session token in the **Authorization** header and returns information from the token holder.
- Request:
  ```
  {
    status: 200,
    headers:{
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Authorization': 'Bearer <jwt>'
    }
  }
  ```
- Reply
  ```
  {
    status: 200,
    headers:{
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    },
    body:{
      "user": {
        "name": "Eloi",
        "email": "gar.elo@gmail.com"
      }
    }
  }
  ```
#### <a name="deposit"></a> Deposit: POST `/api/v1/user/deposit`
Adds currency specific balance to the token holder account. This endpoint is mainly used to populate the user's account with balance to fetch and transfer.
- Request:
  ```
  {
    status: 200,
    headers:{
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Authorization': 'Bearer <jwt>'
    },
    body:{
      currency: {string} 'EUR'
      amount: {number} 15.5 
    }
  }
  ```
- Reply
  ```
  {
    status: 200,
    headers:{
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    },
    body:{
      "amount": 1051.56,
      "currency": "EUR",
      "updatedAt": "2019-05-26T17:24:15.405Z"
    }
  }
  ```
#### <a name="balance"></a> Balance: POST `/api/v1/user/balance`
Returns the account balance of the token holder. If the account holds balance on more than one currency, these are returned in an array.
- Request:
  ```
  {
    status: 200,
    headers:{
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Authorization': 'Bearer <jwt>'
    }
  }
  ```
- Reply
  ```
  {
    status: 200,
    headers:{
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    },
    body:[
      {
        "amount": 1051.56,
        "currency": "USD",
        "updatedAt": "2019-05-26T17:24:15.405Z"
      },
      {
        "amount": 22.579999999999995,
        "currency": "EUR",
        "updatedAt": "2019-05-26T16:27:40.875Z"
      }
    ]
  }
  ```

#### <a name="transfer"></a> Transference: POST `/api/v1/user/transfer`
Attempts to transfer funds from the token holder account to the account linked to `dest_email` email. The request expects `currency`, `balance` and `dest_email`. Concept is optional.
Returns the new balance of the token holder's account.

If the balance is not sufficient or the destination account does not exist in the system, the server will reply with an error and rollback any balance changes.
- Request:
  ```
  {
    status: 200,
    headers:{
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Authorization': 'Bearer <jwt>'
    },
    body:{
      currency: {string} 'EUR'
      amount: {number} 15.5,
      concept: {string?} Netflix,
      dest_email: {string} destUser@mail.com
    }
  }
  ```
- Reply
  ```
  {
    status: 200,
    headers:{
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    },
    body:{
      "amount": 1050,
      "currency": "USD",
      "updatedAt": "2019-05-26T17:27:59.416Z"
    }
  }
  ```

## Notes
### Lines of development
This implementation is but a proof of concept. Each and every feature implemented can, and should, be extended with extra security steps, data validation and so. Here is a brief list of features not implemented due to lack of development time.

- Any operation that uses balance/currency should validate that the currency is ISO accepted as well as attach its symbol and exchange rate.
- Once attempting to transfer funds, if the account holder is trying to transfer a balance in a currency he/she does not hold or does not have enough, the server could attempt to exchange funds from one currency to another.
- An execution date could be added into the transaction object. Allowing for scheduled transactions or periodic operations.
- The destination account and concept of a transaction should be validated against account blacklist.
- Unit and integration tests must be added to enforce maintainability of the codebase and debugging.
- The server architecture could be split into a web interface that accepts incoming requests and outputs actions into a message queue. These actions would then be picked up several other workers each tasked with specific roles. This architecture would facilitate horizontal scaling and load balancing.