FROM node:10.15.3
WORKDIR /usr/src/app

ENV PORT=3000

COPY . /usr/src/app

RUN npm install --production

EXPOSE $PORT

ENTRYPOINT ["npm", "start"]